# tester-matching-frontend


**IMPORTANT**

To run the project you need to have NPM installed on your local machine.

Google Chrome is prefered for running the application.

# Run

1. Clone project
2. Enter root directory of the project via Comannd Line
4. Install project dependencies with command "npm i"
5. Start project with command "ng serve"
6. Access the application in your browser (Google Chrome prefered) - localhost:4200
7. Make sure that back-end part of the application is also running

# Usage

1. Choose at least 1 country and 1 device from dropdowns located in the upper part of screen (check if the back-end is running in case of dropdowns missing data to choose)
2. Press "Search button"
3. Table with results will appear below panel containing dropdowns and search button