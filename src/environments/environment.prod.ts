export const environment = {
  production: true,
  testerMatchingApiUrl: 'http://localhost:8080',
  matchTesters: '/search-tester-device-bug-count',
  allDevices: '/devices',
  allCountries: '/countries'
};
