import {inject, TestBed} from '@angular/core/testing';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Overlay} from '@angular/cdk/overlay';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TesterMatchingService} from '../tester-matching.service';

describe('TesterMatchingService', () => {
  let testerMatchingService: TesterMatchingService;
  let httpMock;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TesterMatchingService,
        MatSnackBar,
        Overlay
      ],
      imports: [
        HttpClientTestingModule
      ]
    });
    testerMatchingService = TestBed.get(TesterMatchingService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(TesterMatchingService).toBeTruthy();
  });

  it('should call httpclient get method (/devices endpoint)', () => {
    testerMatchingService.loadTesters([], []);
    expect(httpMock.expectOne('http://localhost:8080/search-tester-device-bug-count').request.method).toEqual('GET');
  });
});
