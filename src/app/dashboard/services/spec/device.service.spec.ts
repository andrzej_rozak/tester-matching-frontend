import {CountryService} from '../country.service';
import {inject, TestBed} from '@angular/core/testing';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Overlay} from '@angular/cdk/overlay';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {DeviceService} from '../device.service';

describe('DeviceService', () => {
  let deviceService: DeviceService;
  let httpMock;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeviceService,
        MatSnackBar,
        Overlay
      ],
      imports: [
        HttpClientTestingModule
      ]
    });
    deviceService = TestBed.get(DeviceService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(deviceService).toBeTruthy();
  });

  it('should call httpclient get method (/devices endpoint)', () => {
    deviceService.loadDevices();
    expect(httpMock.expectOne('http://localhost:8080/devices').request.method).toEqual('GET');
  });
});
