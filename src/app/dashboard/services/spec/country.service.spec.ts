import {CountryService} from '../country.service';
import {TestBed} from '@angular/core/testing';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Overlay} from '@angular/cdk/overlay';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('CountryService', () => {
  let countryService: CountryService;
  let httpMock;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CountryService,
        MatSnackBar,
        Overlay
      ],
      imports: [
        HttpClientTestingModule
      ]
    });
    countryService = TestBed.get(CountryService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(countryService).toBeTruthy();
  });

  it('should call httpclient get method (/countries endpoint)', () => {
    countryService.loadCountries();
    expect(httpMock.expectOne('http://localhost:8080/countries').request.method).toEqual('GET');
  });
});
