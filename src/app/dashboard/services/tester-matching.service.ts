import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, EMPTY, Observable, Subscription} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {switchMap, tap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TesterMatchResponse} from '../models/tester-match-response';

@Injectable()
export class TesterMatchingService implements OnDestroy {
  private loadSubscription: Subscription;

  private _testersSubject: BehaviorSubject<TesterMatchResponse[]> = new BehaviorSubject([]);
  private _isLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _loadDataSubject: BehaviorSubject<TesterMatchResponse[]> = new BehaviorSubject([]);

  public loading$ = this._isLoadingSubject.asObservable();

  constructor(private httpClient: HttpClient,
              private matSnackBar: MatSnackBar) {
  }

  ngOnDestroy(): void {
    this.loadSubscription?.unsubscribe();
  }

  connect(): Observable<TesterMatchResponse[]> {
    return this._testersSubject.asObservable();
  }

  loadTesters(countries: string[], deviceIdList: number[]): void {
    this.loadSubscription = this._loadDataSubject.pipe(
      tap(() => this._isLoadingSubject.next(true)),
      switchMap(() => this.httpClient.get(`${environment.testerMatchingApiUrl}/${environment.matchTesters}`, {
        params: {
          countryList: countries,
          deviceIdList: deviceIdList.map(String)
        }
      }))
    ).subscribe((testers: TesterMatchResponse[]) => {
        this._testersSubject.next(testers);
        this._isLoadingSubject.next(false);
      },
      () => this.matSnackBar.open('Countries loading failed, please try again later', 'OK', {duration: 5000}));
  }
}
