import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {Country} from '../models/country';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {switchMap, tap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class CountryService implements OnDestroy {
  private loadSubscription: Subscription;

  private _countriesSubject: BehaviorSubject<Country[]> = new BehaviorSubject([]);
  private _isLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _loadDataSubject: BehaviorSubject<Country[]> = new BehaviorSubject([]);

  public loading$ = this._isLoadingSubject.asObservable();

  constructor(private httpClient: HttpClient,
              private matSnackBar: MatSnackBar) {
  }

  connect(): Observable<Country[]> {
    return this._countriesSubject.asObservable();
  }

  ngOnDestroy(): void {
    this.loadSubscription?.unsubscribe();
  }

  loadCountries(): void {
    this.loadSubscription = this._loadDataSubject.pipe(
      tap(() => this._isLoadingSubject.next(true)),
      switchMap(() => this.httpClient.get(`${environment.testerMatchingApiUrl}/${environment.allCountries}`))
    ).subscribe((countries: Country[]) => {
        this._countriesSubject.next(countries);
        this._isLoadingSubject.next(false);
      },
      () => this.matSnackBar.open('Countries loading failed, please try again later', 'OK', {duration: 5000}));
  }
}
