import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {Device} from '../models/device';
import {HttpClient} from '@angular/common/http';
import {switchMap, tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class DeviceService implements OnDestroy {
  private loadSubscription: Subscription;

  private _devicesSubject: BehaviorSubject<Device[]> = new BehaviorSubject([]);
  private _isLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _loadDataSubject: BehaviorSubject<Device[]> = new BehaviorSubject([]);

  public loading$ = this._isLoadingSubject.asObservable();

  constructor(private httpClient: HttpClient,
              private matSnackBar: MatSnackBar) {
  }

  ngOnDestroy(): void {
    this.loadSubscription?.unsubscribe();
  }

  connect(): Observable<Device[]> {
    return this._devicesSubject.asObservable();
  }

  loadDevices(): void {
    this.loadSubscription = this._loadDataSubject.pipe(
      tap(() => this._isLoadingSubject.next(true)),
      switchMap(() => this.httpClient.get<Device[]>(`${environment.testerMatchingApiUrl}/${environment.allDevices}`))
    ).subscribe((devices: Device[]) => {
        this._devicesSubject.next(devices);
        this._isLoadingSubject.next(false);
      },
      () => this.matSnackBar.open('Devices loading failed, please try again later', 'OK', {duration: 5000}));
  }
}
