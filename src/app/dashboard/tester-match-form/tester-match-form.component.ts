import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DeviceService} from '../services/device.service';
import {CountryService} from '../services/country.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Device} from '../models/device';
import {Country} from '../models/country';
import {FormResultEvent} from '../models/form-result-event';

@Component({
  selector: 'app-tester-match-form',
  templateUrl: './tester-match-form.component.html',
  styleUrls: ['./tester-match-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TesterMatchFormComponent {

  @Input() devices: Device[];
  @Input() countries: Country[];
  @Output() onFormSubmit: EventEmitter<FormResultEvent> = new EventEmitter();

  form: FormGroup = this.fb.group({
    countries: ['', Validators.required],
    devices: ['', Validators.required]
  });

  constructor(private matSnackBar: MatSnackBar,
              private fb: FormBuilder) {
  }

  handleDevicesChange(devices: Device[]): void {
    this.form.get('devices').setValue(devices);
  }

  handleCountriesChange(countries: Country[]): void {
    this.form.get('countries').setValue(countries);
  }

  submitForm() {
    this.onFormSubmit.emit({
      devices: this.form.get('devices').value,
      countries: this.form.get('countries').value
    });
  }
}
