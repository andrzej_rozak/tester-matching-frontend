import {NgModule} from '@angular/core';
import {FormBuilder, ReactiveFormsModule} from '@angular/forms';
import {TesterMatchFormComponent} from './tester-match-form.component';
import {MaterialModule} from '../../material.module';
import {SelectWithChipsModule} from '../select-with-chips/select-with-chips.module';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [
    TesterMatchFormComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    SelectWithChipsModule
  ],
  exports: [
    TesterMatchFormComponent
  ],
  providers: [
    FormBuilder
  ]
})
export class TesterMatchFormModule {
}
