import {SortByExperienceDescPipe} from '../sort-by-experience-desc.pipe';
import {TesterMatchResponse} from '../../models/tester-match-response';
import {Tester} from '../../models/tester';
import {Device} from '../../models/device';

describe('SortByExperienceDescPipe', () => {
  const pipe = new SortByExperienceDescPipe();

  it('should sort testers by experience DESC', () => {
    const testers: TesterMatchResponse[] = [
      {
        tester: {} as Tester,
        device: {} as Device,
        bugCount: 123,
        experience: 100
      },
      {
        tester: {} as Tester,
        device: {} as Device,
        bugCount: 123,
        experience: 300
      },
      {
        tester: {} as Tester,
        device: {} as Device,
        bugCount: 123,
        experience: 200
      }];
    expect(pipe.transform(testers).map(tester => tester.experience)).toEqual([300, 200, 100]);
  });
});
