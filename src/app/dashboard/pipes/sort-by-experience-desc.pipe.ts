import {Pipe, PipeTransform} from '@angular/core';
import {TesterMatchResponse} from '../models/tester-match-response';

@Pipe({name: 'sortByExperienceDescPipe'})
export class SortByExperienceDescPipe implements PipeTransform {

  transform(values: TesterMatchResponse[]): TesterMatchResponse[] {
    return values.sort(
      (a: TesterMatchResponse, b: TesterMatchResponse) => a.experience > b.experience ? -1 : a.experience < b.experience ? 1 : 0
    );
  }
}
