import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import {DashboardComponent} from './dashboard.component';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from '../material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {SortByExperienceDescPipe} from './pipes/sort-by-experience-desc.pipe';
import {DeviceService} from './services/device.service';
import {TesterMatchingService} from './services/tester-matching.service';
import {CountryService} from './services/country.service';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let testerMatchingService: TesterMatchingService;
  let countryService: CountryService;
  let deviceService: DeviceService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        SortByExperienceDescPipe
      ],
      imports: [
        HttpClientModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule
      ],
      providers: [
        DeviceService,
        TesterMatchingService,
        CountryService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    testerMatchingService = TestBed.get(TesterMatchingService);
    countryService = TestBed.get(CountryService);
    deviceService = TestBed.get(DeviceService);
    spyOn(testerMatchingService, 'loadTesters');
    spyOn(deviceService, 'loadDevices');
    spyOn(countryService, 'loadCountries');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load countries and devices on init', () => {
    expect(countryService.loadCountries).toHaveBeenCalled();
    expect(deviceService.loadDevices).toHaveBeenCalled();
  });
});

