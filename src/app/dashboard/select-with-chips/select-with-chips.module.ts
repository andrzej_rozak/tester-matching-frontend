import {NgModule} from '@angular/core';
import {MaterialModule} from '../../material.module';
import {SelectWithChipsComponent} from './select-with-chips.component';
import {CommonModule} from '@angular/common';
import {DisplayDescriptionPipe} from './pipes/display-description.pipe';

@NgModule({
  declarations: [
    SelectWithChipsComponent,
    DisplayDescriptionPipe
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    SelectWithChipsComponent
  ]
})
export class SelectWithChipsModule {
}
