import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatSelectChange} from '@angular/material/select';

@Component({
  selector: 'app-select-with-chips',
  templateUrl: './select-with-chips.component.html',
  styleUrls: ['./select-with-chips.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectWithChipsComponent {

  @Input() itemsToChoose: any[];
  @Input() displayNameField: string;
  @Input() placeholder: string;

  @Output() onItemsChange: EventEmitter<any[]> = new EventEmitter<any[]>();

  selectedItems: any[] = [];

  handleSelectionChange(item: any): void {
    this.removeItemFromItemsToSelectList(item);
    this.selectedItems.push(item);
    this.emitSelectedItems();
  }

  unselectItem(item: any): void {
    const index = this.selectedItems.indexOf(item);
    this.selectedItems.splice(index, 1);
    this.itemsToChoose.push(item);
    this.emitSelectedItems();
  }

  removeItemFromItemsToSelectList(item: any): void {
    const index = this.itemsToChoose.indexOf(item);
    this.itemsToChoose.splice(index, 1);
  }

  emitSelectedItems(): void {
    this.onItemsChange.emit(this.selectedItems);
  }
}
