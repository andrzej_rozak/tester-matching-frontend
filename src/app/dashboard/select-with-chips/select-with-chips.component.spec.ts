import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectWithChipsComponent } from './select-with-chips.component';
import {By} from '@angular/platform-browser';
import {DisplayDescriptionPipe} from './pipes/display-description.pipe';
import {DashboardComponent} from '../dashboard.component';
import {MaterialModule} from '../../material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SortByExperienceDescPipe} from '../pipes/sort-by-experience-desc.pipe';
import {AppModule} from '../../app.module';

describe('SelectWithChipsComponent', () => {
  let component: SelectWithChipsComponent;
  let fixture: ComponentFixture<SelectWithChipsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        SelectWithChipsComponent,
      ],
      imports: [
        MaterialModule,
        BrowserAnimationsModule,
        AppModule
      ],
      providers: [
        DisplayDescriptionPipe,
        SortByExperienceDescPipe
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectWithChipsComponent);
    component = fixture.componentInstance;
    component.itemsToChoose = ['item2', 'item1', 'item3'];
    fixture.detectChanges();

    // activate dropdown so options are displayed
    const dropdown = fixture.debugElement.query(By.css('mat-select')).nativeElement;
    dropdown.click();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should remove item from itemsToChoose and add it to selectedItems after selection change', () => {
    // given
    const options = fixture.debugElement.queryAll(By.css('mat-option')).map(el => el.nativeElement);

    // when
    options[1].click();
    fixture.detectChanges();

    const item1ExistsInItemsToChoose = component.itemsToChoose.find((el: string) => el === 'item1');
    const item1ExistsInSelectedItems = component.selectedItems.find((el: string) => el === 'item1');

    // then
    expect(item1ExistsInItemsToChoose).toBeUndefined();
    expect(item1ExistsInSelectedItems).toBeTruthy();
  });

  it('should display mat-chip for chosen elements', () => {
    // given
    const options = fixture.debugElement.queryAll(By.css('mat-option')).map(el => el.nativeElement);

    // when
    options[0].click();
    options[1].click();
    fixture.detectChanges();


    // then
    const chips = fixture.debugElement.queryAll(By.css('mat-chip')).map(el => el.nativeElement);

    expect(chips.length).toBe(2);
  });

  it('should display item in dropdown if is of type string', () => {
    // given
    const options = fixture.debugElement.queryAll(By.css('.mat-option-text')).map(el => el.nativeElement);


    // then
    expect(options[0].innerHTML).toContain('item2');
    expect(options[1].innerHTML).toContain('item1');
    expect(options[2].innerHTML).toContain('item3');
  });

  it('should display item\'s name field in dropdown if is of type object and list should be sorted', () => {
    // given
    component.itemsToChoose = [
      { nameField: 'item1' },
      { nameField: 'item2' },
      { nameField: 'item3' },
    ];
    component.displayNameField = 'nameField';
    const dropdown = fixture.debugElement.query(By.css('mat-select')).nativeElement;

    // when
    dropdown.click();
    fixture.detectChanges();

    const options = fixture.debugElement.queryAll(By.css('.mat-option-text')).map(el => el.nativeElement);

    // then
    expect(options[0].innerHTML).toContain('item1');
    expect(options[1].innerHTML).toContain('item2');
    expect(options[2].innerHTML).toContain('item3');
  });
});
