import {DisplayDescriptionPipe} from '../display-description.pipe';

describe('DisplayDescriptionPipe', () => {
  const pipe = new DisplayDescriptionPipe();

  it('should print primitive value if value is primitive', () => {
    expect(pipe.transform(1)).toBe('1');
    expect(pipe.transform('text')).toBe('text');
    expect(pipe.transform(true)).toBe('true');
  });

  it('should print object property specified in parameter', () => {
    const obj = { id: 1, text: 'text-value', flag: false};
    expect(pipe.transform(obj, 'text')).toBe('text-value');
  });

});
