import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'displayDescriptionPipe'})
export class DisplayDescriptionPipe implements PipeTransform {

  transform(value: any, descriptionField?: string): string {
    return typeof value !== 'object' ? value.toString() :
      (value && descriptionField) ? value[descriptionField] : '';
  }
}
