import {Device} from './device';
import {Country} from './country';

export interface FormResultEvent {
  devices: Device[];
  countries: Country[];
}
