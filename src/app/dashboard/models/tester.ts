export interface Tester {
  testerId: number;
  firstName: string;
  lastName: string;
  country: string;
  lastLogin: Date;
}
