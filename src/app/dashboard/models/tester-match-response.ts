import {Tester} from './tester';
import {Device} from './device';

export interface TesterMatchResponse {
  tester: Tester;
  device: Device;
  bugCount: number;
  experience: number;
}
