import {NgModule} from '@angular/core';
import {DashboardComponent} from './dashboard.component';
import {HttpClientModule} from '@angular/common/http';
import {MaterialModule} from '../material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SortByExperienceDescPipe} from './pipes/sort-by-experience-desc.pipe';
import {DeviceService} from './services/device.service';
import {CountryService} from './services/country.service';
import {TesterMatchingService} from './services/tester-matching.service';
import {TesterMatchFormModule} from './tester-match-form/tester-match-form.module';


@NgModule({
  declarations: [
    DashboardComponent,
    SortByExperienceDescPipe,
  ],
  imports: [
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule,
    TesterMatchFormModule
  ],
  exports: [
    DashboardComponent
  ],
  providers: [
    DeviceService,
    CountryService,
    TesterMatchingService,
  ]
})
export class DashboardModule {
}
