import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Device} from './models/device';
import {MatSnackBar} from '@angular/material/snack-bar';
import {DeviceService} from './services/device.service';
import {CountryService} from './services/country.service';
import {Country} from './models/country';
import {TesterMatchingService} from './services/tester-matching.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
  readonly displayedColumns: string[] = ['firstName', 'lastName', 'device', 'bugCount', 'experience'];



  constructor(private matSnackBar: MatSnackBar,
              public deviceService: DeviceService,
              public countryService: CountryService,
              public testerMatchingService: TesterMatchingService) {
  }

  ngOnInit() {
    this.deviceService.loadDevices();
    this.countryService.loadCountries();
  }

  handleFormSubmit(event: {devices: Device[], countries: Country[]}): void {
    if (!event) {
      this.matSnackBar.open('Please choose country and device', 'OK', {duration: 5000});
    } else {
      this.testerMatchingService
        .loadTesters(event.countries.map(c => c.name), event.devices.map(device => device.deviceId));
    }
  }
}
